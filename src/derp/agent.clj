(ns derp.agent
  (:require [clojure.data.json :as json]
            [clojure.set :as set]
            [clojure.string :as str]
            [org.httpkit.client :as http]
            [plumbing.core :refer :all]
            [taoensso.timbre :as log])
  (:import (java.io ByteArrayInputStream InputStream)
           (java.net URL)
           (java.util Base64)))

(defn encode-body [body]
  (cond
    (string? body)
    (.encodeToString (Base64/getEncoder) (.getBytes ^String body))
    :else
    (throw (ex-info "Cannot encode body." {:body body}))))

(defn ^InputStream decode-body [^String b64]
  (ByteArrayInputStream. (.decode (Base64/getDecoder) b64)))

(defn- request-url-parts [uri]
  (let [url (URL. (str "http://derp" uri))]
    {:query-string (.getQuery url)
     :scheme (.getProtocol url)
     :server-name (.getHost url)
     :server-port (.getPort url)
     :uri (.getPath url)}))

(defn- derp-request->ring-request
  [{:keys [body headers method remoteAddress uri] :as derp-request}]
  (when derp-request
    (merge {:body (decode-body body)
            :headers (map-keys str/lower-case headers)
            :remote-addr remoteAddress
            :request-method (-> method str/lower-case keyword)}
           (request-url-parts uri))))

(defn- ring-response->derp-response [ring-response request-id]
  (when ring-response
    (assert (:body ring-response) "Response must include :body.")
    (assert (:status ring-response) "Response must include :status.")
    (-> ring-response
        (set/rename-keys {:status :statusCode})
        (select-keys [:body :headers :statusCode])
        (update :body encode-body)
        (assoc :id request-id)
        json/write-str)))

(defn- handle-server-response [{:keys [body error status]}]
  (log/info "Server hit complete. Status" status)
  (try
    (cond
      ;; error, it's unclear what to do here because it's just loopin'
      error
      (do
        (log/error error "Error during request.")
        (log/info "Waiting before trying again.")
        (Thread/sleep 5000)
        nil)
      ;; timeout, just hit it again
      (= 408 status)
      (do
        (log/debug "Server hit timed out.")
        nil)
      ;; some other unexpected status
      (not= 200 status)
      (do
        (log/error "Unexpected server response. Status:" status "Body:" body)
        (log/info "Waiting before trying again.")
        (Thread/sleep 5000)
        nil)
      ;; handle it
      :else
      (->> body json/read-str (map-keys keyword)))
    (catch Exception e
      (log/error e "While trying to handle server response.")
      nil)))

(defn- client-request [url response]
  (log/info "Hitting server.")
  (let [opts {:as :text
              :body response
              :headers {"Content-Type" "application/json"}}]
    (handle-server-response @(http/post url opts))))

(defn- safe-handle [handler {request-id :id :as derp-request}]
  (try
    (some-> derp-request
            derp-request->ring-request
            handler
            (ring-response->derp-response request-id))
    (catch Throwable e
      (log/error e "Unexpected error while handling.")
      nil)))

(defn- derp-agent [handler continue {:keys [poll-url]}]
  (log/info "Starting agent.")
  (loop [response nil]
    (if @continue
      (let [derp-request (client-request poll-url response)]
        (recur (safe-handle handler derp-request)))
      (log/info "Stopping agent."))))

(defn- poll-url [url agent-id]
  (str url "/poll/" agent-id))

(defn start [url agent-id handler opts]
  (let [continue (atom true)
        a (future (derp-agent handler
                              continue
                              (merge opts
                                     {:poll-url (poll-url url agent-id)})))]
    (fn []
      (reset! continue false)
      @a)))
