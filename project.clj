(defproject derp-agent-clj "0.0.1-SNAPSHOT"
  :description "A data exchange reverse proxy (DERP) agent for clj."
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[com.taoensso/timbre "4.10.0"]
                 [compojure "1.6.1"]
                 [http-kit "2.3.0"]
                 [org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.6"]
                 [prismatic/plumbing "0.5.5"]])
